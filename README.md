# Website

This repository is used to create a static website for Matters of the Mind Therapy, LLC.

It uses the Material for MkDocs set of themes and plugins, and the MkDocs static site generator (SSG) to create a simple marketing site.

Note: due to patient privacy concerns, this website is strictly a marketing site for the practice. It contains no PHI/PII, and it does not process payments.
