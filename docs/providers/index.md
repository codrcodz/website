# Providers

<figure markdown>
![Jasmine Scott-Cochran](assets/provider.jpg){ height="256" width="256" loading="lazy" }
  <center>
    <figcaption markdown>
    [Jasmine Scott-Cochran, MS, NCC, LPC](jasmine.md)
    </figcaption>
  </center>
</figure>
