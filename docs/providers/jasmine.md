# Jasmine Scott-Cochran

<figure markdown>
  ![Jasmine Scott-Cochran](assets/provider.jpg){ height="256" width="256" loading="lazy" }
  <figcaption>
    Jasmine Scott-Cochran, MS, NCC, LPC
  </figcaption>
</figure>

Jasmine has worked in the field of psychology for nearly 20 years. She has wide-ranging experience working in multiple settings including: several adolescent residental treatment facilities like the [New Life Children's Center](https://upbring.org/new-life-childrens-center/), treatment programs for the neurodivergent like the [Autism Treatment Center of San Antonio](https://www.atcoftexas.org/san-antonio.html), psychiatric hospitals like the [Alaska Psychiatric Institute](https://dfcs.alaska.gov/api/Pages/default.aspx), integrative behavioral healthcare facilities like the [UT Health - Robert B. Green Campus](https://www.uthscsa.edu/patient-care/physicians/location/Robert-B-Green-Campus-Downtown), and private practice. She opened Matters of the Mind Therapy, LLC in 2023.

As your counselor, Jasmine will use her rich behavioral-focused and integrative healthcare backgrounds to inform her therapeutic approach. She will work with you to create a strengths-based, goal-oriented, treatment program that is specialized to meet your individual needs.

## Education

- Master of Science (MS) in Clinical Mental Health Counseling from the University of Texas San Antonio (UTSA)

## Memberships

- [Member](https://iocdf.org/providers/scott-cochran-jasmine-d/) of the International OCD Foundation

## Licenses

- [Licensed Professional Counselor (LPC)](https://vo.licensing.hpc.texas.gov/datamart/searchByName.do) (License No. 83366) by the State of Texas
- [National Certified Counselor (NCC)](1140314) (License No. 1140314) by the National Board of Certified Counselors (NBCC)

## Certifications

- Exposure & Response Prevention (ERP) Therapy from the [Behavioral Therapy Training Institute (BTTI)](https://iocdf.org/professionals/training-institute/btti/)

## Methods

- [Prolonged Exposure Therapy (PET)](https://www.psychologytoday.com/us/therapy-types/prolonged-exposure-therapy)
- [Cognitive Processing Therapy (CPT)](https://www.psychologytoday.com/us/therapy-types/cognitive-processing-therapy)
- [Dialectial Behavioral Therapy (DBT)](https://www.psychologytoday.com/us/therapy-types/dialectical-behavior-therapy)
- [Cognitive Behavioral Therapy (CBT)](https://www.psychologytoday.com/us/basics/cognitive-behavioral-therapy)
- [Acceptance & Commitment Therapy (ACT)](https://www.psychologytoday.com/us/therapy-types/acceptance-and-commitment-therapy)
- [Moviational Interviewing (MI)](https://www.psychologytoday.com/us/therapy-types/motivational-interviewing)
- [Strength-Based Therapy](https://www.psychologytoday.com/us/therapy-types/strength-based-therapy)
- [Internal Family Systems (IFS) Therapy](https://www.psychologytoday.com/us/therapy-types/internal-family-systems-therapy) 
- [Exposure & Response Prevention (ERP)](https://www.psychologytoday.com/us/therapy-types/exposure-and-response-prevention)

## Specialties

- [Body-Focused Repetitive Behaviors (BFRB)](https://www.psychologytoday.com/us/basics/body-focused-repetitive-behaviors)
- [Obsessive Compulsive Disorder (OCD)](https://www.psychologytoday.com/us/conditions/obsessive-compulsive-disorder)
- [Personality Disorders](https://www.psychiatry.org/patients-families/personality-disorders/what-are-personality-disorders)
- [Trauma-Related Disorders](https://www.psychologytoday.com/us/basics/trauma)
