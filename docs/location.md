# Location


## Telehealth

All our providers are licensed by state boards to serve clients via telehealth or in-person psychotherapy sessions.
For telehealth services, clients must reside within a state that the therapist is licensed to provide services in.

## In Person

### San Antonio, TX

We are in the process of acquiring office space in North Central San Antonio for in-person sessions.

<div class="google-map">
     <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d5008.612064502143!2d-98.46383475274716!3d29.606315978697353!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sen!2sus!4v1684766258471!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>
