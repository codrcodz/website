# Contact Us

## :fontawesome-solid-circle-exclamation: Emergencies 

If you are experiencing a medical emergency, contact the National 991 System. Please call:

[911](tel:911){ .md-button }

If you are experiencing an acute mental health crisis and are unable to immediately reach a provider, contact the National 988 Suicide & Crisis Lifeline. Please call:

[988](tel:988){ .md-button }

## :fontawesome-solid-paper-plane: Email 

The practice will respond to requests via email within one business day.

[staff@motmtherapy.com](mailto:staff@motmtherapy.com){ .md-button }

Electronic medical records may be sent to us from other providers via email. Please call to confirm receipt.

## :fontawesome-solid-phone: Phone

Please call and leave a voicemail with the practice if you have any questions.

[123-456-7890](tel:1234567890){ .md-button }

If we are unable to answer, please leave a voicemail. We will respond to voicemails within one business day.

## :fontawesome-solid-fax: Fax

Paper medical records may be transferred to the practice via fax.

[123-456-7890](tel:1234567890){ .md-button }

Please call your provider to confirm receipt.
