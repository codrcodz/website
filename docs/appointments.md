# :fontawesome-regular-calendar-days: Appointments

## Testing Only

Clients referred by a psychologist for psychological test administration, should use this option.

[Jasmine Scott-Cochran, MS, NCC, LPC<br>(Testing, XXX minutes)](https://google.com){ .md-button }

Note: we are not certified to interpret the results of psychological tests, only administer them.
After testing is complete, your referring psychologist will review your test and,
depending on the results, provide you with a diagnosis and treatment options.

## New Patients

New patients interested in indivdual or couples psychotherapy sessions, should use this option.

[Jasmine Scott-Cochran, MS, NCC, LPC<br>(Psychotherapy, 90 minutes)](https://google.com){ .md-button }

New patients must complete their inital intake paperwork before being seen by a provider.

## Existing Patients

Existing patients interested in indivdual or couples psychotherapy sessions, should use this option.

[Jasmine Scott-Cochran, MS, NCC, LPC<br>(Psychotherapy, 60 minutes)](https://google.com){ .md-button }
